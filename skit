#!/usr/bin/env python3

import subprocess
import sys
import textwrap
import re


last_voice = 'Alex'


def get_voices():
    bytes = subprocess.check_output(['say', '-v', '?'])
    output_text = bytes.decode()
    pattern = re.compile(r'(\w+)\s+(\w+)\s+# (.+)\s*$', re.MULTILINE)
    voices_info = pattern.findall(output_text)
    voices = set(voice for voice, language, message in voices_info)
    return voices


def parse_line(line):
    global last_voice

    if line.startswith(' ') or line.startswith('\t'):
        voice = last_voice
        message = line
    else:
        voice, message = line.split(': ', 1)

    last_voice = voice
    return voice, message


def speak(line):
    valid_voices = get_voices()
    print(line)
    voice, message = parse_line(line)
    tokens = ['say', '-r', '220', '-v', voice, message]

    if voice in valid_voices:
        subprocess.call(tokens)
    else:
        input('  > ')


if __name__ == '__main__':
    if len(sys.argv) > 1:
        script = sys.argv[1]
    else:
        print('Syntax: skit [ script_file ]')
        sys.exit(1)

    with open(script) as f:
        for line in f:
            line = line.rstrip()
            if line and not line.startswith('#'):
                speak(line)
            else:
                print(line)
