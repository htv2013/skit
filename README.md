# Introduction

**skit** is a simple script with simulate a skit with multiple voices: human's and computer voice alike.

# Getting Started

1. Click download from a box in the right hand side to download a zip file to your computer.
2. Unzip that file into a directory (folder)
3. `cd` into the folder and issue the following command:

```
python skit script.txt
```

# Using skit

To use skit, create a script file, such as one below (script.txt):

	Bruce: Hello Vicki
	Vicki: Hello Bruce. It is a beautiful day, isn't it?
	Bruce: It sure is.

Then, from the terminal, issue the following command:

	python skit script.txt

**skit** will then convert the text within *script.txt* to speech, using appropriate voices.

If **skit** sees a voice that it does not recognize, it will assume the voice is of a real human. So, it will display that line, wait for the human to read it. After reading the line, the user should hit Return to continue.

# List of Voices

You can see a list of voices skit recognizes by issuing the following command at the terminal:

	say -v ?

For reference, please see all_voices.txt
